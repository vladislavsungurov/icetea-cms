var walk = require('walk'),
    fs = require('fs'),
    async = require('async'),
    hbs = require('hbs');

module.exports.loadViews = function (app, cb) {
    var directory = __dirname + "/views";
    var options = {
        followLinks : false
    };
    
    var walker = walk.walk(directory, options);
    var views = [];
    
    walker.on('file', function (root, file, next) {
        var fileName = root +  "/" + file.name;
        var name = fileName.replace(directory + "/", '');
        console.log(name);
        fs.readFile(fileName, "utf8", function (err, data) {
            if (err) 
                console.log(err);
            else
                hbs.registerPartial(name, data);
            
            next();
        });
    });
    
    walker.on('end', function () {
        app.set('partials', views);
        cb();
    });
}

module.exports.loadMiddlewares = function (app, cb) {
    console.log("Please wait...we are loading middlewares")
    var directory = __dirname + "/middlewares";
    var options = {
        followLinks : false
    };
    
    var walker = walk.walk(directory, options);
    var middlewares = [];
    walker.on("file", function (root, file, next) {
        var fileName = root + "/" + file.name;
        var name = file.name.replace(".js", "");
        var middleware = require(fileName);
        middlewares[name] = middleware;
        next();
    });
    
    walker.on('end', function () {
       console.log(middlewares);
       app.set("middlewares", middlewares);
       cb(); 
    });
}

module.exports.loadModules = function (app, cb) {
    console.log("And we are loading modules too");
    
    var directory = __dirname + "/modules";
    var options = {
        followLinks: false
    };
    
    var modules = [];
    var models = {};
    var walker = walk.walk(directory, options);
    
    walker.on("directory", function (root, dir, next) {
        if (root == directory) {
            modules.push(root + "/" + dir.name);
        }
        
        next();
    });
    
    walker.on('end', function () {
        async.forEach(modules, function (item, callback) {
            var modelsDir = item + "/models";
            var connection = app.get('db');
            var modelsWalker = walk.walk(modelsDir, options);
            modelsWalker.on('file', function (root, file, next) {
                var fileName = file.name;
                var name = fileName.replace('.js', '');
                var schema = require(root + "/" + fileName);
                
                models[name] = connection.model(name, schema);
                next();
            });
            
            modelsWalker.on('end', function () {
                app.set("models", models);
                var routesDir = item + "/routes";
                var routesWalker = walk.walk(routesDir, options);
                var middlewares = app.get("middlewares");
                
                routesWalker.on('file', function (root, file, next) {
                    var dir = root.replace(routesDir + "/", "");
                    var dirs = dir.split('/');
                    
                    if (dirs.length > 0 && dirs[0] == "security") {
                        if (dirs.length == 2) {
                            var role = dirs[1];
                            var fileName = file.name;
                            var controller = require(root + "/" + fileName);
                            var keys = Object.keys(controller);
                            
                            for (var i = 0; i < keys.length; i++) {
                                var strs = keys[i].split("#");
                                var method = strs[0];
                                
                                if (!strs[1])
                                    strs[1] = "";
                                    
                                var route = "/" + strs[1];
                                var func = controller[keys[i]];
                                
                                if (method == 'get')
                                    app.get(route, middlewares[role], middlewares['utils'], func);
                                else if (method == 'post')
                                    app.post(route, middlewares[role], middlewares['utils'], func);
                            }
                        }
                    }
                    else {
                        var fileName = file.name;
                        var controller = require(root + "/" + fileName);
                        var keys = Object.keys(controller);
                        
                        
                        for (var i = 0; i < keys.length; i++) {
                            var strs = keys[i].split("#");
                            var method = strs[0];
                             
                            if (!strs[1])
                                strs[1] = "";
                                 
                            var route = "/" + strs[1];
                            var func = controller[keys[i]];
                            
                            if (method == 'get')
                                app.get(route, middlewares['utils'], func);
                            else if (method == 'post')
                                app.post(route, middlewares['utils'], func);
                        }
                    }
                    
                    next();
                });
                
                routesWalker.on('end', function () {
                    callback();
                });
            }); 
        }, function () {
            console.log("Modules routes are registered");
            cb();
        });
    });
}

