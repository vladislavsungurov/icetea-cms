module.exports = {
    "get#": function (req,res) {
        res.send("Hello anonymous!");
    },
    
    "get#view": function (req, res) {
        res.render("index.hbs", { title: "Hello, world!", viewName : "index.hbs" });
    },
    
    "get#json": function (req, res) {
        res.json({ msg : "Hello, world! This is JSON!"});
    }
}
