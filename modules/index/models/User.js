var mongoose = require('mongoose');

var Schema = new mongoose.Schema({
    username: {
        type: String,
        unqiue: true,
    },
    password: {
        type: String,  
    },
    role: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unqiue: true
    },
    createdAt: {
        type: Date,
        required: true,
        "default": Date.now(),
    }
});

module.exports = Schema;