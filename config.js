var mongoose = require('mongoose');

module.exports.DEFAULT_PORT = 10500;
module.exports.DEFAULT_ADDR = "0.0.0.0";
module.exports.CONNECTION = mongoose.createConnection("mongodb://localhost/nxtto");