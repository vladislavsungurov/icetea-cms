module.exports = function (req, res, next) {
    if (!req.isAuthenticated())
        return res.send(401);
        
    if (req.user || req.isAuthenticated()) {
        if (req.user.role != "admin")
            res.send(401);
        else
            next();
    }
    else
        res.send(401);
}
