module.exports = function (req, res, next) {
    if (req.isAuthenticated())
        return res.send(401);
    else
        return next();
}