var express = require('express'),
    config = require('./config.js'),
    utils = require('./utils.js'),
    passport = require('passport'),
    connect = require('connect'),
    RedisStore = require('connect-redis')(connect),
    strategies = require('./strategies.js'),
    hbs = require('hbs');
    
var store  = new RedisStore();
var secret = "qsnld1kKUNIUKB@OI91BK3J1B ?>KWE1U0";
var key = "99f590fa9c950ffb5422b05266816928";
app = express();

var hbs = require('hbs');

app.configure(function() {
    app.set('port', config.DEFAULT_PORT);
    app.set('addr', config.DEFAULT_ADDR);
    app.set('db', config.CONNECTION);
    app.set('models', []);
    
    app.use(function (req, res, next) {
       req.models = app.get('models');
       req.passport = passport;
       next(); 
    });
    
    app.set('view engine', 'hbs');
    app.set('views', __dirname + '/views');
    
    app.use(express.static(__dirname + '/public'));
    app.use(express.cookieParser(secret));
    app.use(express.bodyParser());
    app.use(express.session({store : store, secret : secret}));
    app.use(passport.initialize());
    app.use(passport.session()); 
    app.use(express.logger());
    app.use(app.router);
});

utils.loadViews(app, function () {
    utils.loadMiddlewares(app, function () {
        utils.loadModules(app, function () {
            var models = app.get('models');
            strategies.serializers(passport, models['User']);
            strategies.LocalStrategy(models['User'], passport);
            
            app.listen(app.get('port'), app.get("addr"), function () {
                console.log("Server started at " + app.get("addr") + ":" + app.get("port"));
            }); 
       }); 
    });  
});