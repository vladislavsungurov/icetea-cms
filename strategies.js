var LocalStrategy = require('passport-local').Strategy;

module.exports.serializers = function (passport, userModel) {
    passport.serializeUser(function (user, done) {
       done(null, user._id); 
    });
    
    passport.deserializeUser(function (id, done) {
       userModel.findById(id, function (err, user) {
          done(err, user); 
       });
    });
}

module.exports.LocalStrategy = function (userModel, passport) {
    passport.use(new LocalStrategy(function (username, password, done) {
        userModel.findOne({username : username}, function (err, user) {
           if (err) {
               return done(err);
           } 
           if (!user) {
               return done(null, false, { message : 'Incorrect username or password'});
           }
           user.verifyPassword(password, function (r) {
              if (r)
                return done(null, user);
              else
                return done(null, false , { message : '2Incorrect username or password.'});
           });
        });
    }));
}